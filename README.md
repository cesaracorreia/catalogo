## Sistema de Cátálogo de Produtos

# 1. Como compilar e executar a aplicação
mvn spring-boot:run

# 2. Exemplos de cada chamada da api

1. Recuperar todos os Produtos excluindo os relacionamentos;
http://localhost:8080/produto/listaTodosSemRelacionamentos

2. Recuperar todos os Produtos incluindo um relacionamento específico (Produto ou Imagem);
nao implementado - tempo insuficiente

3. Igual ao nº 1 utilizando um id de produto específico;
http://localhost:8080/produto/produtoSemRelacionamentos/1

4. Igual ao nº 2 utilizando um id de produto específico;
nao implementado - tempo insuficiente

5. Recupera a coleção de produtos filhos por um id de produto específico;
http://localhost:8080/produto/listaProdutosFilhos/1

6. Recupera a coleção de Imagens para um id de produto específico;
http://localhost:8080/produto/imagensPorProduto/1

# 3. Como executar a suíte de testes automatizados
mvn test