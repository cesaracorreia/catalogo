package br.com.montreal.catalogo.domain.enums;

public enum TipoImagem {
	PNG, GIF, JPEG, BMP
}
