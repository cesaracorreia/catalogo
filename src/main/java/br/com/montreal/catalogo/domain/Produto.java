package br.com.montreal.catalogo.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.montreal.framework.domain.BasicEntity;

@Entity
@Table(name = "produtos")
public class Produto extends BasicEntity {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String nome;
	private String descricao;
	private Long idProdutoPai;
	private List<Imagem> imagens;

	public Produto() {

	}

	public Produto(Long id, String nome, String descricao) {
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
	}

	@Override
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "nome")
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Column(name = "descricao")
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Column(name = "id_produto_pai")
	public Long getIdProdutoPai() {
		return idProdutoPai;
	}

	public void setIdProdutoPai(Long idProdutoPai) {
		this.idProdutoPai = idProdutoPai;
	}

	@OneToMany(mappedBy = "produto", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonManagedReference
	public List<Imagem> getImagens() {
		return imagens;
	}

	public void setImagens(List<Imagem> imagens) {
		this.imagens = imagens;
	}

}
