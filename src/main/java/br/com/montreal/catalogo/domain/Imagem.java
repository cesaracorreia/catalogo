package br.com.montreal.catalogo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.montreal.catalogo.domain.enums.TipoImagem;
import br.com.montreal.framework.domain.BasicEntity;

@Entity
@Table(name = "imagens")
public class Imagem extends BasicEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private TipoImagem tipo;
	private byte[] bytes;
	private Produto produto;

	@Override
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "tipo")
	public TipoImagem getTipo() {
		return tipo;
	}

	public void setTipo(TipoImagem tipo) {
		this.tipo = tipo;
	}

	@Column(name = "bytes")
	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_produto")
	@JsonBackReference
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

}
