package br.com.montreal.catalogo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.montreal.catalogo.domain.Imagem;
import br.com.montreal.catalogo.domain.Produto;
import br.com.montreal.catalogo.service.ImagemService;
import br.com.montreal.catalogo.service.ProdutoService;
import br.com.montreal.framework.controller.BasicController;
import br.com.montreal.framework.system.BasicException;

@RestController
@RequestMapping("/produto")
public class ProdutoController extends BasicController<Produto, Long> {

	@Autowired
	private ProdutoService produtoService;

	@Autowired
	private ImagemService imagemService;

	@Override
	protected ProdutoService getService() {
		return produtoService;
	}

	@Override
	@PostMapping("/save")
	public Produto save(@RequestBody Produto entity) throws BasicException {
		return getService().save(entity);
	}

	@Override
	@PostMapping("/saveList")
	public List<Produto> save(@RequestBody List<Produto> saveList) throws BasicException {
		return getService().saveList(saveList);
	}

	@Override
	@GetMapping("/{id}")
	public Produto load(@PathVariable("id") Long id) throws BasicException {
		return getService().load(id);
	}

	@GetMapping("/listaTodosSemRelacionamentos")
	public List<Produto> listaTodosSemRelacionamentos() throws BasicException {
		return getService().findProdutosLight();
	}

	@GetMapping("/listaProdutosFilhos/{idProdutoPai}")
	public List<Produto> listaProdutosFilhos(@PathVariable("idProdutoPai") Long idProdutoPai) throws BasicException {
		return getService().findProdutosFilhosLight(idProdutoPai);
	}

	@GetMapping("/produtoSemRelacionamentos/{id}")
	public Produto listaTodosSemRelacionamentos(@PathVariable("id") Long id) throws BasicException {
		return getService().findProdutoLight(id);
	}

	@GetMapping("/imagensPorProduto/{id}")
	public List<Imagem> listaImagensPorProduto(@PathVariable("id") Long idProduto) throws BasicException {
		return imagemService.findImagensPorProduto(idProduto);
	}

	@Override
	@DeleteMapping("/deleteByIds")
	public void delete(@RequestBody List<Long> ids) throws BasicException {
		super.delete(ids);
	}

	@Override
	@DeleteMapping("/delete")
	public void delete(@RequestBody Produto produto) throws BasicException {
		super.delete(produto);
	}

	@Override
	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable Long id) throws BasicException {
		getService().delete(id);
	}

}
