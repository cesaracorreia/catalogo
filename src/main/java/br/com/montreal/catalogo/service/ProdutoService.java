package br.com.montreal.catalogo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.montreal.catalogo.data.IProdutoDao;
import br.com.montreal.catalogo.domain.Produto;
import br.com.montreal.framework.service.BasicService;
import br.com.montreal.framework.system.BasicException;

@Service
public class ProdutoService extends BasicService<Produto, Long> {

	@Autowired
	private IProdutoDao produtoDao;

	@Override
	protected IProdutoDao getDao() {
		return produtoDao;
	}

	@Override
	protected Produto beforeSave(Produto produto) {
		Long id = -1l;
		if (produto.getId() != null) {
			id = produto.getId();
		}

		Produto p = produtoDao.findByName(id, produto.getNome());
		if (p != null) {
			throw new BasicException("Ja existe um produto com este nome no banco de dados.");
		}
		return produto;
	}

	public List<Produto> findProdutosLight() {
		return produtoDao.findProdutosLight();
	}

	public Produto findProdutoLight(Long id) {
		return produtoDao.findProdutoLight(id);
	}

	public List<Produto> findProdutosFilhosLight(Long idProdutoPai) {
		return produtoDao.findProdutosFilhosLight(idProdutoPai);
	}

}
