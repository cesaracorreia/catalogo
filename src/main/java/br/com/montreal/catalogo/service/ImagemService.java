package br.com.montreal.catalogo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.montreal.catalogo.data.IImagemDao;
import br.com.montreal.catalogo.domain.Imagem;
import br.com.montreal.framework.service.BasicService;

@Service
public class ImagemService extends BasicService<Imagem, Long> {

	@Autowired
	private IImagemDao imagemDao;

	@Override
	protected IImagemDao getDao() {
		return imagemDao;
	}

	public List<Imagem> findImagensPorProduto(Long idProduto) {
		return imagemDao.findImagensPorProduto(idProduto);
	}

}
