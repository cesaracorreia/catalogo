package br.com.montreal.catalogo.data;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.montreal.catalogo.domain.Imagem;
import br.com.montreal.framework.data.ISpringDao;

public interface IImagemDao extends ISpringDao<Imagem, Long> {

	@Query("select i from Imagem i where i.produto.id = :idProduto")
	List<Imagem> findImagensPorProduto(@Param("idProduto") Long idProduto);
}
