package br.com.montreal.catalogo.data;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.montreal.catalogo.domain.Produto;
import br.com.montreal.framework.data.ISpringDao;

@Repository
public interface IProdutoDao extends ISpringDao<Produto, Long> {

	@Query("select p from Produto p where p.nome = :nome and p.id <> :id")
	Produto findByName(@Param("id") Long id, @Param("nome") String nome);

	@Query("select new Produto(p.id, p.nome, p.descricao) from Produto p ")
	List<Produto> findProdutosLight();

	@Query("select new Produto(p.id, p.nome, p.descricao) from Produto p where idProdutoPai = :idProdutoPai")
	List<Produto> findProdutosFilhosLight(@Param("idProdutoPai") Long idProdutoPai);

	@Query("select new Produto(p.id, p.nome, p.descricao) from Produto p where p.id = :id")
	Produto findProdutoLight(@Param("id") Long id);

}
