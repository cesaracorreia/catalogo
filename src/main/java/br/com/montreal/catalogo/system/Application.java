package br.com.montreal.catalogo.system;

import java.util.ArrayList;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.hsqldb.jdbc.JDBCDataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import br.com.montreal.catalogo.domain.Imagem;
import br.com.montreal.catalogo.domain.Produto;
import br.com.montreal.catalogo.domain.enums.TipoImagem;
import br.com.montreal.catalogo.service.ProdutoService;

@SpringBootApplication
@EnableAutoConfiguration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "br.com.montreal.catalogo.data" })
@ComponentScan(basePackages = { "br.com.montreal.catalogo.service", "br.com.montreal.catalogo.controller" })
@EntityScan(basePackages = { "br.com.montreal.catalogo.domain" })
@Configuration
public class Application {

	private static final Logger LOG = Logger.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void popuateDatabase() {
		Produto escritorio = new Produto();
		escritorio.setNome("Escritório");
		escritorio.setDescricao("Material de escritório");
		Imagem img = new Imagem();
		img.setTipo(TipoImagem.PNG);
		img.setProduto(escritorio);
		escritorio.setImagens(new ArrayList<>());
		escritorio.getImagens().add(img);
		produtoService().save(escritorio);

		Produto caneta = new Produto();
		caneta.setNome("Caneta");
		caneta.setDescricao("Esferografica");
		img = new Imagem();
		img.setTipo(TipoImagem.PNG);
		img.setProduto(caneta);
		caneta.setImagens(new ArrayList<>());
		caneta.getImagens().add(img);
		caneta.setIdProdutoPai(escritorio.getId());
		produtoService().save(caneta);

		Produto papel = new Produto();
		papel.setNome("Papel");
		papel.setDescricao("Papel A4");
		img = new Imagem();
		img.setTipo(TipoImagem.PNG);
		img.setProduto(papel);
		papel.setImagens(new ArrayList<>());
		papel.getImagens().add(img);
		papel.setIdProdutoPai(escritorio.getId());
		produtoService().save(papel);

		Produto cozinha = new Produto();
		cozinha.setNome("Cozinha");
		cozinha.setDescricao("Produtos para cozinha");
		img = new Imagem();
		img.setTipo(TipoImagem.PNG);
		img.setProduto(cozinha);
		cozinha.setImagens(new ArrayList<>());
		cozinha.getImagens().add(img);
		produtoService().save(cozinha);

		Produto lixeira = new Produto();
		lixeira.setNome("Lixeira");
		papel.setDescricao("Lixeira inox");
		img = new Imagem();
		img.setTipo(TipoImagem.PNG);
		img.setProduto(lixeira);
		lixeira.setImagens(new ArrayList<>());
		lixeira.getImagens().add(img);
		img = new Imagem();
		img.setTipo(TipoImagem.PNG);
		img.setProduto(lixeira);
		lixeira.getImagens().add(img);
		lixeira.setIdProdutoPai(escritorio.getId());
		produtoService().save(lixeira);
	}

	@Bean
	public ProdutoService produtoService() {
		return new ProdutoService();
	}

	@Bean
	public DataSource dataSource() {
		JDBCDataSource ds = new JDBCDataSource();
		ds.setUrl("jdbc:hsqldb:mem:catalogo");
		ds.setUser("sa");
		ds.setPassword("");

		return ds;
	}

	@Bean
	JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		return transactionManager;
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		DataSource datasource = dataSource();
		sessionFactory.setDataSource(datasource);
		sessionFactory.setPackagesToScan(new String[] { "br.com.montreal.catalogo.domain" });
		sessionFactory.setHibernateProperties(hibernateProperties());
		return sessionFactory;
	}

	public Properties hibernateProperties() {
		return new Properties() {
			{
				setProperty("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");
				setProperty("hibernate.show_sql", "true");
				setProperty("hibernate.hbm2ddl.auto", "none");
			}
		};
	}

}
