package br.com.montreal.framework.system;

import java.util.List;

import org.apache.log4j.Logger;

/**
 * The Class BasicException.
 * 
 * @author Cesar Correia
 */
public class BasicException extends RuntimeException {

	/** The Constant log. */
	protected static final Logger log = Logger.getLogger(BasicException.class);

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The raw message. */
	private List<String> errorMessages;

	/**
	 * Instantiates a new basic exception.
	 */
	public BasicException() {
	}

	/**
	 * This constructor MUST BE PRIVATE. Use errorHandling instead.
	 * 
	 * @param t
	 *            the t
	 */
	public BasicException(Throwable t) {
		super(t);
	}

	/**
	 * Instantiates a new basic exception.
	 *
	 * @param message
	 *            the message
	 */
	public BasicException(String message) {
		super(message);
	}

	public BasicException(String message, Throwable t) {
		super(message, t);
	}

	public List<String> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

}
