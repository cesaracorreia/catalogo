package br.com.montreal.framework.domain;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Basic bean to represent a entity model.
 * 
 * @author Cesar Correia
 * @param <ID>
 *            the generic type
 */
@MappedSuperclass
public abstract class BasicEntity implements IBasicEntity, Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5112346718792831664L;

	/** For persistence processing logic. For concurrent store processing. */
	@JsonIgnore
	private int version;

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	@Override
	@Version
	public int getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 * 
	 * @param version
	 *            the new version
	 */
	@Override
	public void setVersion(int version) {
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if ((obj == null) || !(obj.getClass().equals(this.getClass()))) {
			return false;
		}

		if (this == obj) {
			return true;
		}

		final BasicEntity dto = (BasicEntity) obj;

		if ((getId() != null) && (dto.getId() != null)) {
			if (getId().equals(dto.getId())) {
				return true;
			}
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}

		return getId().hashCode();
	}

}
