package br.com.montreal.framework.domain;

import java.io.Serializable;

/**
 * Basic entity to represent a entity model.
 * 
 * @author Cesar Correia
 * @param <ID>
 *            the generic type
 */
public interface IBasicEntity {

	public Serializable getId();

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public int getVersion();

	/**
	 * Sets the version.
	 * 
	 * @param version
	 *            the new version
	 */
	public void setVersion(int version);

}
