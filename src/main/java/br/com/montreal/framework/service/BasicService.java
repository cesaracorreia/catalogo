package br.com.montreal.framework.service;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import br.com.montreal.framework.data.IDao;
import br.com.montreal.framework.domain.IBasicEntity;
import br.com.montreal.framework.system.BasicException;

/**
 * The Class StandardBusinessObject.
 *
 * @author Cesar Correia
 * 
 *         <b>Event Model Implementation for Model Tier.</b>
 * 
 * @param <B>
 *            the generic type
 * @param <ID>
 *            the generic type
 */
@Transactional(readOnly = true)
public abstract class BasicService<E extends IBasicEntity, ID extends Serializable> {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(BasicService.class);

	protected abstract IDao<E, ID> getDao();

	@Transactional(readOnly = false, rollbackFor = { BasicException.class, Exception.class })
	public E save(E entity) {
		try {
			entity = this.beforeSave(entity);
			getDao().save(entity);
			return this.afterSave(entity);
		} catch (final BasicException e) {
			throw e;
		} catch (final Exception e) {
			LOG.error("Error saving object", e);
			throw new BasicException("Error saving object", e);
		}
	}

	protected E beforeSave(E entity) {
		return entity;
	}

	/**
	 * After insert.
	 *
	 * @param uc
	 *            the uc
	 * @param entity
	 *            the entity
	 * @throws BasicException
	 *             the basic exception
	 */
	protected E afterSave(E entity) {
		return entity;
	}

	@Transactional(readOnly = false, rollbackFor = { BasicException.class, Exception.class })
	public List<E> saveList(List<E> entities) {
		try {
			return getDao().save(entities);
		} catch (final BasicException e) {
			throw e;
		} catch (final Exception e) {
			LOG.error("Error saving objects", e);
			throw new BasicException("Error saving objects", e);
		}
	}

	@Transactional(readOnly = false, rollbackFor = { BasicException.class, Exception.class })
	public void delete(E entity) {
		try {
			getDao().delete(entity);
		} catch (final BasicException e) {
			throw e;
		} catch (final Exception e) {
			LOG.error("Error deleting object", e);
			throw new BasicException("Error deleting object", e);
		}
	}

	@Transactional(readOnly = false, rollbackFor = { BasicException.class, Exception.class })
	public void delete(ID id) {
		try {
			getDao().delete(id);
		} catch (final BasicException e) {
			throw e;
		} catch (final Exception e) {
			LOG.error("Error deleting object", e);
			throw new BasicException("Error deleting object", e);
		}
	}

	@Transactional(readOnly = false, rollbackFor = { BasicException.class, Exception.class })
	public void delete(List<ID> ids) {
		try {
			getDao().delete(ids);
		} catch (final BasicException e) {
			throw e;
		} catch (final Exception e) {
			LOG.error("Error deleting object", e);
			throw new BasicException("Error deleting object", e);
		}
	}

	public E load(ID id) {
		try {
			id = beforeLoad(id);
			return afterLoad(getDao().load(id));
		} catch (final BasicException e) {
			throw e;
		} catch (final Exception e) {
			LOG.error("Error loading object", e);
			throw new BasicException("Error loading object", e);
		}
	}

	private ID beforeLoad(ID id) {
		return id;
	}

	private E afterLoad(E entity) {
		return entity;
	}

	public List<E> loadAll() {
		try {
			beforeLoadAll();
			return afterLoadAll(getDao().loadAll());
		} catch (final BasicException e) {
			throw e;
		} catch (final Exception e) {
			LOG.error("Error loading objects", e);
			throw new BasicException("Error loading objects", e);
		}
	}

	private void beforeLoadAll() {

	}

	private List<E> afterLoadAll(List<E> entities) {
		return entities;
	}

}
