package br.com.montreal.framework.controller;

import java.io.Serializable;
import java.util.List;

import br.com.montreal.framework.domain.IBasicEntity;
import br.com.montreal.framework.service.BasicService;
import br.com.montreal.framework.system.BasicException;

public abstract class BasicController<E extends IBasicEntity, ID extends Serializable> {
	protected abstract BasicService<E, ID> getService();

	public E save(E entity) throws BasicException {
		return getService().save(entity);
	}

	public List<E> save(List<E> saveList) throws BasicException {
		return getService().saveList(saveList);
	}

	public E load(ID id) throws BasicException {
		return getService().load(id);
	}

	public List<E> loadAll() throws BasicException {
		return getService().loadAll();
	}

	public void delete(List<ID> ids) throws BasicException {
		getService().delete(ids);
	}

	public void delete(E entity) throws BasicException {
		getService().delete(entity);
	}

	public void delete(ID id) throws BasicException {
		getService().delete(id);
	}

}
