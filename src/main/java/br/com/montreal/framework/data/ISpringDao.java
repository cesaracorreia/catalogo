package br.com.montreal.framework.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.montreal.framework.domain.IBasicEntity;

@Repository
public interface ISpringDao<E extends IBasicEntity, ID extends Serializable> extends IDao<E, ID>, JpaRepository<E, ID> {

	@Override
	default List<E> save(List<E> entities) {
		List<E> saveList = new ArrayList<>();
		Iterable<E> it = save(entities);
		for (E e : it) {
			saveList.add(e);
		}
		return saveList;
	}

	@Override
	default E load(ID id) {
		return findOne(id);
	}

	@Override
	default List<E> loadAll() {
		List<E> list = new ArrayList<>();
		Iterable<E> it = findAll();
		for (E e : it) {
			list.add(e);
		}
		return list;
	}

	@Override
	@Query("delete from #{#entityName} t where t.id in (:ids)")
	void delete(@Param("ids") List<ID> ids);

}
