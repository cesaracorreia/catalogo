package br.com.montreal.framework.data;

import java.io.Serializable;
import java.util.List;

import br.com.montreal.framework.domain.IBasicEntity;

public interface IDao<E extends IBasicEntity, ID extends Serializable> {

	E save(E entity);

	List<E> save(List<E> entities);

	E load(ID id);

	boolean exists(ID id);

	List<E> loadAll();

	long count();

	void delete(ID id);

	void delete(List<ID> ids);

	void delete(E entity);

}
