package br.com.montreal.catalogo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.montreal.catalogo.domain.Produto;
import br.com.montreal.catalogo.system.ApplicationTest;
import br.com.montreal.framework.system.BasicException;

@RunWith(SpringRunner.class)
// @SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Import(ApplicationTest.class)
public class ProdutoServiceTest {

	@Autowired
	private ProdutoService service;

	@Test
	public void testaInclusao() {

		Produto escritorio = new Produto();
		escritorio.setNome("Escritório");
		escritorio.setDescricao("Material de escritório");

		escritorio = service.save(escritorio);

		assertThat(escritorio).isNotNull();
		if (escritorio != null) {
			assertThat(escritorio.getId()).isNotNull();
			if (escritorio.getId() != null) {
				Produto p = service.load(escritorio.getId());
				assertThat(p).isNotNull();
			}
		}
	}

	@Test
	public void testaDuplicidade() {
		Produto cozinha = new Produto();
		cozinha.setNome("Cozinha");
		cozinha.setDescricao("Produtos de Cozinha");

		cozinha = service.save(cozinha);

		assertThat(cozinha).isNotNull();
		if (cozinha != null) {
			Produto cozinha2 = new Produto();
			cozinha2.setNome("Cozinha");
			cozinha2.setDescricao("Produtos de Cozinha");
			try {
				Produto p = null;
				service.save(cozinha2);
				assertThat(p).isNull();
			} catch (BasicException e) {
				assertEquals(e.getMessage(), "Ja existe um produto com este nome no banco de dados.");
			}
		}
	}

}
