package br.com.montreal.catalogo.system;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.hsqldb.jdbc.JDBCDataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import br.com.montreal.catalogo.service.ProdutoService;

@SpringBootApplication
@EnableAutoConfiguration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "br.com.montreal.catalogo.data" })
@ComponentScan(basePackages = { "br.com.montreal.catalogo.service", "br.com.montreal.catalogo.controller" })
@EntityScan(basePackages = { "br.com.montreal.catalogo.domain" })
@Configuration
public class ApplicationTest {

	private static final Logger LOG = Logger.getLogger(ApplicationTest.class);

	public static void main(String[] args) {
		SpringApplication.run(ApplicationTest.class, args);
	}

	@Bean
	public ProdutoService produtoService() {
		return new ProdutoService();
	}

	@Bean
	public DataSource dataSource() {
		JDBCDataSource ds = new JDBCDataSource();
		ds.setUrl("jdbc:hsqldb:mem:catalogo");
		ds.setUser("sa");
		ds.setPassword("");

		return ds;
	}

	@Bean
	JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		return transactionManager;
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		DataSource datasource = dataSource();
		sessionFactory.setDataSource(datasource);
		sessionFactory.setPackagesToScan(new String[] { "br.com.montreal.catalogo.domain" });
		sessionFactory.setHibernateProperties(hibernateProperties());
		return sessionFactory;
	}

	public Properties hibernateProperties() {
		return new Properties() {
			{
				setProperty("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");
				setProperty("hibernate.show_sql", "true");
				setProperty("hibernate.hbm2ddl.auto", "none");
			}
		};
	}

}